pngexifinfo
===========

Show the EXIF information embedded in a PNG file.

This implementation is experimental and reflects the proposed
specifications currently discussed by the PNG Development Group.

Sample usage
------------

```sh
# Show the EXIF info inside a PNG file.
$ /path/to/pngexifinfo.py /path/to/file.png

# Show the EXIF info inside a plain EXIF file.
# Show the EXIF tags in base 16.
$ /path/to/pngexifinfo.py --hex /path/to/file.exif

# Show the help text.
$ /path/to/pngexifinfo.py --help
```
